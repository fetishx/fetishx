fetish
===================

[TOC]

----


BDSM-FETISH swap to "fetish"
-----

*BDSM-FETISH is finished as an altcoin, so one last throw of the dice, and the view is change it back to "fetish" and forget about its once sister coin, BDSM. Fetish was more popular anyway.*

----

Coins Minted for swap
-----

**400,000**

In other words, there will be 400,000 fetish, available to exchange BDSM-FETISH for.

The rate is 1 = 1

(actual start is 403,000 which includes 3,000 test mined coins and staked coins, which have been thrown into the swap pot)

-----

Nova Exchange
-----

A swap will be undertaken at [Nova Exchange](https://novaexchange.com)


-----

Hybrid period
-----

There are circa 12,500 blocks that can be mined, POW. Last block is at height 14,000. 

The reward is 35 coins a block.

-----



New wallet
-----

You can download the new, nicer, wallet here:


[Bitbucket](https://bitbucket.org/fetishx/fetish-qt/downloads/)




-----

Explorer
-----

[fetish explorer](http://explorer.fetishx.info:3001)


plus the old:


[BDSM-FETISH explorer](http://explorer.bdsm-fetish.net:3001)


-----

Tech stuff
-----

- Coin: fetish
- Ticker: FETSH
- Algo: x11 hybrid to full PoS
- RPC: 9079
- PORT: 9078
- 5% interest
- Minimum staking: 12 hours
- Maximum staking: 14 days

-----

For the hybrid period
-----

- 480 second block targets
- difficulty adjusts each block
- 15 confirmations for mined coins
- 35 coins reward


-----

Swap figures (reiterated)
-----

> 400,000 premined for giveaway
> 
> 
> 1 WHIPPED = 1 FETSH

Any remaining FETSH, to be redistributed to those that have already claimed.

This is simply to avoid burning coins, as it confuses people regarding available coins.

----

BDSM-FETISH: "Destroyed and lost coin stats"
-----

> - 290,000 BDSM-FETISH purposely destroyed,
- another 20,000 accidently destroyed,
- 21,000 wallet.dat file accidently deleted,
- lost wallet.dat of circa 8,000 (these are just from me!)


-----

Mining and coin count
-----
> 400,000 for swap (plus the surplus of 3,000 test mined and staked)
> 
Then, hopefully to breath life into fetish:


>- 12,500 remaining hybrid blocks @ 35 coins per block, which realistically, with both POW and POS mining blocks during the hybrid period, would add at most 100,000 if it is mined. 

>- NB: As noted above, I have test mined some, and test staked, so have added those coins onto the 400,000 FETSH for swap.
>

Coins

>- Total coin count after the hybrid period is finished will probably be in the region of 500,000. That is, 400,000 from the premine/swap, and 100,000 POW and POS mined blocks.

----

Changes from BDSM-FETISH
-----

> -  Disabled irc calls
> -  Pretty picture on the wallet, otherwise the same source (clone) as BDSM-FETISH
> -  Obviously with a new genesis block, tx hash, ports, messages et al.
> -  Interest: 5%
> -  Addresses start with a small "f"
> -  Possibility to mine the first hybrid blocks

-----

Logo
-----

The old fetish logo is back!


![fetish logo](https://cdn.pbrd.co/images/2Btfq8HE.png)


Wallet
-----


The wallet features this lady as background:


![fetish wallet](https://cdn.pbrd.co/images/2m1Y7QSkt.png)


-------

**BDSM-FETISH is finished.**
Goodbye!